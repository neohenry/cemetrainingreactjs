import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {
	createStore,
	applyMiddleware,
	compose /* compose is optional */,
} from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "./reducers";

import App from "./components/App";

// Below is without redux devtool ext installed
//const store = createStore(albumsReducer, applyMiddleware(thunkMiddleware));

// Below is with redux dev tool ext installed
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
	rootReducer,
	composeEnhancers(applyMiddleware(thunkMiddleware))
);

console.log("store is ", store.getState());

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
