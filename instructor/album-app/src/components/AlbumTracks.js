import React from "react";

const AlbumTracks = ({ visible = true, tracks }) => {
	return (
		visible && (
			<p>{tracks} tracks</p>
			// <ol className="album-tracks">
			// 	{props.tracks.map((track) => (
			// 		<li key={track}>{track}</li>
			// 	))}
			// </ol>
		)
	);
};

export default AlbumTracks;
