import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Formik } from "formik";

//import axios from "axios";
import { addAlbum } from "../actions";

//const CreateAlbumForm = ({ fetchAlbum, setFetchAlbum, addAlbum }) => {
const CreateAlbumForm = ({ fetchAlbum, setFetchAlbum }) => {
	const dispatch = useDispatch();
	const history = useHistory();

	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h3>Add New Album</h3>
			<Formik
				initialValues={{ title: "", artist: "", price: "", tracks: "" }}
				validate={(values) => {
					const errors = {};
					if (!values.title) {
						errors.title = "Title is required";
					}

					if (!values.artist) {
						errors.artist = "Artist is required";
					}

					if (!values.price) {
						errors.price = "Price is required";
					}

					if (!values.tracks) {
						errors.tracks = "Tracks is required";
					}

					return errors;
				}}
				onSubmit={async (values, { setSubmitting }) => {
					await dispatch(
						addAlbum(
							{
								title: values.title,
								artist: values.artist,
								price: values.price,
								tracks: values.tracks,
							},
							fetchAlbum,
							setFetchAlbum
						)
					);
					setSubmitting(false);
					history.push("/");
				}}
			>
				{({
					values,
					errors,
					touched,
					handleChange,
					handleBlur,
					handleSubmit,
					isSubmitting,
				}) => (
					<form onSubmit={handleSubmit}>
						<label htmlFor="title">Title:</label>
						<input
							type="text"
							name="title"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.title}
						/>
						{errors.title && touched.title && errors.title}
						<br />
						<label htmlFor="artist">Artist:</label>
						<input
							type="text"
							name="artist"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.artist}
						/>
						{errors.artist && touched.artist && errors.artist}
						<br />
						<label htmlFor="price">Price:</label>
						<input
							type="number"
							name="price"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.price}
						/>
						{errors.price && touched.price && errors.price}
						<br />
						<label htmlFor="tracks">Tracks:</label>
						<input
							type="text"
							name="tracks"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.tracks}
						/>
						{errors.tracks && touched.tracks && errors.tracks}
						<br />
						<button type="submit" disabled={isSubmitting}>
							Submit
						</button>
					</form>
				)}
			</Formik>
		</div>
	);

	/* const [title, setTitle] = useState("");
	const [artist, setArtist] = useState("");
	const [price, setPrice] = useState("");
	const [tracks, setTracks] = useState("");

	const handleSubmit = async (e) => {
		e.preventDefault();

		await dispatch(
			addAlbum(
				{
					title: title,
					artist: artist,
					price: price,
					tracks: tracks,
				},
				fetchAlbum,
				setFetchAlbum
			)
		);
		history.push("/");
	};

	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h3>Add New Album</h3>

			<form onSubmit={handleSubmit} autoComplete="off">
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="title">Title:</label>
						<input
							id="title"
							type="text"
							className="form-control"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="artist">Artist:</label>
						<input
							id="artist"
							type="text"
							className="form-control"
							value={artist}
							onChange={(e) => setArtist(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="price">Price:</label>
						<input
							id="price"
							type="number"
							className="form-control"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="tracks">Number of tracks:</label>
						<input
							id="tracks"
							type="number"
							className="form-control"
							value={tracks}
							onChange={(e) => setTracks(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Create Album"
							className="btn btn-outline-secondary"
						/>
					</div>
				</div>
			</form>
		</div>
	); */
};

// export default connect(null, { addAlbum })(CreateAlbumForm);
export default CreateAlbumForm;
