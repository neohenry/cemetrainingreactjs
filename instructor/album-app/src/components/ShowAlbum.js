import React from "react";
import { useSelector } from "react-redux";

import AlbumTitle from "./AlbumTitle";
import AlbumArtist from "./AlbumArtist";
import AlbumTracks from "./AlbumTracks";

const ShowAlbum = () => {
	const album = useSelector((state) => state.albums.show_entity);
	return (
		<div className="container">
			<div className="row">
				<div className="col-md-4">
					<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
						<div className="card-header">Show Album</div>
						<div className="card-body">
							<AlbumTitle title={album.title} />
							<AlbumArtist artist={album.artist} />
							<AlbumTracks tracks={album.tracks} />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ShowAlbum;
