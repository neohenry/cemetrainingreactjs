import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { setShowAlbum } from "../actions";

import AlbumTitle from "./AlbumTitle";
import AlbumArtist from "./AlbumArtist";
import AlbumTracks from "./AlbumTracks";
import ShowHideButton from "./ShowHideButton";

const Album = ({ id, title, artist, tracks }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [visible, setVisibity] = useState(true);

	const handleShowAlbum = () => {
		dispatch(setShowAlbum({ id, title, artist, tracks }));
		history.push("/show");
	};

	return (
		<div className="col-md-4">
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<AlbumTitle title={title} />
					<AlbumArtist artist={artist} />
					<AlbumTracks tracks={tracks} visible={visible} />
					<ShowHideButton toggle={setVisibity} visible={visible} />
					<button
						className="btn btn-sm btn-outline-secondary ml-1"
						onClick={handleShowAlbum}
					>
						Show
					</button>
				</div>
			</div>
		</div>
	);
};

export default Album;
