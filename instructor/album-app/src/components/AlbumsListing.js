import React from "react";
import Album from "./Album";
import { connect, useSelector } from "react-redux";

const AlbumsListing = () => {
	const albums = useSelector((state) => state.albums.entities);
	const error = useSelector((state) => state.albums.error);
	const loading = useSelector((state) => state.albums.loading);

	if (error) {
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}

	if (loading) {
		return (
			<div className="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}

	return (
		<div className="container">
			<div className="row">
				{albums.map((album) => (
					<Album
						key={album.id}
						id={album.id}
						title={album.title}
						artist={album.artist}
						tracks={album.tracks}
					/>
				))}
			</div>
		</div>
	);
};

// refactor below to useSelector
// const mapStateToProps = (state) => {
// 	return {
// 		albums: state.albums.entities,
// 		error: state.albums.error,
// 		loading: state.albums.loading,
// 	};
// };

// export default connect(mapStateToProps)(AlbumsListing);

export default AlbumsListing;
