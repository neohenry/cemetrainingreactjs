import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import AlbumTitle from "../../components/AlbumTitle";

let container = null;
beforeEach(() => {
	// setup a DOM element as a render target
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	// cleanup on exiting
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders with or without a title", () => {
	act(() => {
		render(<AlbumTitle />, container);
	});
	expect(container.textContent).toBe("");
	debugger;
	act(() => {
		render(<AlbumTitle title="Title 1" />, container);
	});
	expect(container.textContent).toBe("Title 1");
});
