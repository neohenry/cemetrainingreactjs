import {
	ADD_ALBUM_BEGIN,
	FETCH_ALBUMS_BEGIN,
	ADD_ALBUM_SUCCESS,
	FETCH_ALBUMS_SUCCESS,
	ADD_ALBUM_FAILURE,
	FETCH_ALBUMS_FAILURE,
	SET_SHOW_ALBUM,
} from "../actions/types";

const initialState = {
	entities: [],
	loading: false,
	error: null,
	show_entity: null,
};

export default (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in albumsReducer`);
	switch (action.type) {
		case FETCH_ALBUMS_BEGIN:
		case ADD_ALBUM_BEGIN:
			return { ...state, loading: true, error: null };
		case FETCH_ALBUMS_SUCCESS:
			return { ...state, entities: action.payload, loading: false };
		case FETCH_ALBUMS_FAILURE:
			return { ...state, entities: [], loading: false, error: action.payload };
		case ADD_ALBUM_SUCCESS:
			return { ...state, loading: false };
		case ADD_ALBUM_FAILURE:
			return { ...state, loading: false, error: action.payload };
		case SET_SHOW_ALBUM:
			return { ...state, show_entity: action.payload };
		default:
			return state;
	}
};

//state is looking like this
// {
//     albums: {
//         entities: [],
//         loading: false,
//         error: "failed to fetch data"
//     },
//     errors : ["failed to fetch data", "failed to add albums"],
//     user: {
//         name: "john"
//     },
//     cart: []
// }
