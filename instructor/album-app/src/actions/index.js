import axios from "axios";

import {
	ADD_ALBUM_BEGIN,
	FETCH_ALBUMS_BEGIN,
	ADD_ALBUM_SUCCESS,
	FETCH_ALBUMS_SUCCESS,
	ADD_ALBUM_FAILURE,
	FETCH_ALBUMS_FAILURE,
	SET_SHOW_ALBUM,
} from "./types";

// for our Action Creators
export const fetchAlbumsBegin = () => {
	return {
		type: FETCH_ALBUMS_BEGIN,
	};
};

export const fetchAlbumsSuccess = (albums) => {
	return {
		type: FETCH_ALBUMS_SUCCESS,
		payload: albums,
	};
};

export const fetchAlbumsFailure = (err) => {
	return {
		type: FETCH_ALBUMS_FAILURE,
		payload: { message: "Failed to fetch albums.. please try again later" },
	};
};

// to be call by the components
export const fetchAlbums = () => {
	// returns the thunk function
	return (dispatch, getState) => {
		dispatch(fetchAlbumsBegin());
		console.log("state after fetchAlbumsBegin", getState());
		axios.get("http://localhost:8088/albums").then(
			(res) => {
				//console.log(res);
				//setAlbums(res.data); // TODO dispatch FETCH_ALBUMS_SUCCESS
				setTimeout(() => {
					dispatch(fetchAlbumsSuccess(res.data));
					console.log("state after fetchAlbumsSuccess", getState());
				}, 3000);
			},
			(err) => {
				// dispatch FETCH_ALBUMS_FAILURE
				dispatch(fetchAlbumsFailure(err));
				console.log("state after fetchAlbumsFailure", getState());
			}
		);
	};
};

export const addAlbumBegin = () => {
	return {
		type: ADD_ALBUM_BEGIN,
	};
};

export const addAlbumSuccess = () => {
	return {
		type: ADD_ALBUM_SUCCESS,
	};
};

export const addAlbumFailure = (err) => {
	return {
		type: ADD_ALBUM_FAILURE,
		payload: { message: "Failed to add new album.. please try again later" },
	};
};

function delay(t, v) {
	return new Promise(function (resolve) {
		setTimeout(resolve.bind(null, v), t);
	});
}

export const addAlbum = (album) => {
	// returns our async thunk function
	return (dispatch, getState) => {
		return axios.post("http://localhost:8088/albums", album).then(
			() => {
				console.log("album created!");
				// this is where we can dispatch ADD_ALBUM_SUCCESS
				dispatch(addAlbumSuccess());
			},
			(err) => {
				dispatch(addAlbumFailure(err));
				console.log("state after addAlbumFailure", getState());
			}
		);
	};
};

export const setShowAlbum = (album) => {
	return {
		type: SET_SHOW_ALBUM,
		payload: album,
	};
};
